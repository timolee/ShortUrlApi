/**
 *
 * Copyright (C) 2013 陕西威尔伯乐信息技术有限公司
 * http://www.wellbole.com
 *
 * @className:com.wellbole.shorturl.spi.Am126ShortUrlGenerater
 * @description: 基于网易126.am实现的短地址生成器
 *
 * @version:v1.0.0
 * @author:李飞
 *
 * Modification History:
 * Date         Author      Version     Description
 * -----------------------------------------------------------------
 * 2013年10月1日      李飞              v1.0.0        create
 *
 */
package com.wellbole.shorturl.spi;

import java.util.HashMap;
import java.util.Map;

import org.nutz.http.Http;
import org.nutz.json.Json;
import org.nutz.log.Log;
import org.nutz.log.Logs;

import com.wellbole.shorturl.ShortUrlGenerater;
import com.wellbole.shorturl.ShortUrlResult;

/**
 * 基于网易126.am实现的短地址生成器
 * @author 李飞
 */
public class Am126ShortUrlGenerater implements ShortUrlGenerater {
	/**
	 * 日志
	 */
	private static final Log log = Logs.getLog("Am126ShortUrlGenerater");
	
	/**
	 * 网易短地址服务 API Key
	 */
	private String apiKey = "";
	/**
	 * 服务地址
	 */
	private String serviceUrl="http://126.am/api!shorten.action";
	
	/**
	 * 单位毫秒
	 */
	private int timeout = 60 * 1000;

	/* (non-Javadoc)
	 * @see com.wellbole.shorturl.ShortUrlGenerater#generate(java.lang.String)
	 */
	@Override
	public ShortUrlResult generate(String longUrl) {
		//构造生成结果
		ShortUrlResult sur = new ShortUrlResult();
		sur.setLongUrl(longUrl);
		//构造参数
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("key", this.apiKey);
		params.put("longUrl", longUrl);
		//发送请求到服务器
		String apiRetStr = Http.post(this.serviceUrl, params, timeout);
		
		if(null == apiRetStr || apiRetStr.isEmpty()){
			sur.setStatusCode(500);
			sur.setStatusTxt("请求服务器发生错误");
			log.error(sur.toString());
			return sur;
		}
		
		log.debug(apiRetStr);
		//将json转换成Map
		Map<String,String> apiRetMap = Json.fromJsonAsMap(String.class, apiRetStr);
		if(null == apiRetMap){
			sur.setStatusCode(501);
			sur.setStatusTxt("json转换错误");
			log.error(sur.toString());
			return sur;
		}
		sur.setStatusTxt(apiRetMap.get("status_txt"));
		sur.setShortUrl("http://"+apiRetMap.get("url"));
		sur.setStatusCode(Integer.parseInt(apiRetMap.get("status_code")));
		log.info(sur.toString());
		return sur;
	}

	/**
	 * @param apiKey 设定126.im提供的apiKey
	 */
	public final void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

	/**
	 * @param serviceUrl 设定短地址转换服务器 126.im的地址
	 */
	public final void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}

	/**
	 * @param timeout 设定http请求超时时间
	 */
	public final void setTimeout(int timeout) {
		this.timeout = timeout;
	}



	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Am126ShortUrlGenerater sug = new Am126ShortUrlGenerater();
		sug.setApiKey("b4df5122444bb419838aaa");
		ShortUrlResult sur = sug.generate("http://www.wellbole.com/proj/myht/");
		if(sur.isOk()){
			log.info("shortUrl:" + sur.getShortUrl());
		}
	}
}
