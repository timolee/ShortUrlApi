#ShortUrlApi
短地址生成器接口，目前仅实现了网易126.am的短地址生成器接口。<br/>
如何使用：<br/>
Am126ShortUrlGenerater sug = new Am126ShortUrlGenerater();<br/>
//这个apikey需要在126.am网站上申请。<br/>
sug.setApiKey("b4df5122444bb4198387f09");<br/>
//生成指定参数的短地址<br/>
ShortUrlResult sur = sug.generate("http://www.wellbole.com");<br/>
//生成成功，打印出结果<br/>
if(sur.isOk()){<br/>
	log.info("shortUrl:" + sur.getShortUrl());<br/>
}<br/>
